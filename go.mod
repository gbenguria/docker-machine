module github.com/docker/machine

go 1.16

require (
	cloud.google.com/go v0.80.0 // indirect
	github.com/Azure/azure-sdk-for-go v5.0.0-beta+incompatible
	github.com/Azure/go-ansiterm v0.0.0-20170929234023-d6e3b3328b78 // indirect
	github.com/Azure/go-autorest v7.2.1+incompatible
	github.com/aws/aws-sdk-go v1.36.21
	github.com/bugsnag/bugsnag-go v1.0.6-0.20151120182711-02e952891c52
	github.com/bugsnag/osext v0.0.0-20130617224835-0dd3f918b21b // indirect
	github.com/bugsnag/panicwrap v0.0.0-20160118154447-aceac81c6e2f // indirect
	github.com/cenkalti/backoff v0.0.0-20141124221459-9831e1e25c87
	github.com/codegangsta/cli v1.11.1-0.20151120215642-0302d3914d2a
	github.com/davecgh/go-spew v1.1.0 // indirect
	github.com/dgrijalva/jwt-go v3.0.1-0.20160831183534-24c63f56522a+incompatible // indirect
	github.com/digitalocean/godo v1.0.1-0.20170317202744-d59ed2fe842b
	github.com/docker/docker v17.12.0-ce-rc1.0.20180621001606-093424bec097+incompatible
	github.com/docker/go-units v0.2.1-0.20151230175859-0bbddae09c5a // indirect
	github.com/exoscale/egoscale v0.9.23
	github.com/golang/groupcache v0.0.0-20200121045136-8c9f03a8e57e // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/google/go-querystring v0.0.0-20140804062624-30f7a39f4a21 // indirect
	github.com/intel-go/cpuid v0.0.0-20181003105527-1a4a6f06a1c6
	github.com/jinzhu/copier v0.0.0-20180308034124-7e38e58719c3 // indirect
	github.com/jmespath/go-jmespath v0.4.0 // indirect
	github.com/mitchellh/mapstructure v0.0.0-20140721150620-740c764bc614 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/rackspace/gophercloud v1.0.1-0.20150408191457-ce0f487f6747
	github.com/samalba/dockerclient v0.0.0-20151231000007-f661dd4754aa
	github.com/sirupsen/logrus v1.0.4 // indirect
	github.com/skarademir/naturalsort v0.0.0-20150715044055-69a5d87bef62
	github.com/stretchr/objx v0.1.0 // indirect
	github.com/stretchr/testify v1.6.1
	github.com/tent/http-link-go v0.0.0-20130702225549-ac974c61c2f9 // indirect
	github.com/vmware/govcloudair v0.0.2
	github.com/vmware/govmomi v0.26.1
	go.opencensus.io v0.23.0 // indirect
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9
	golang.org/x/net v0.0.0-20210330142815-c8897c278d10
	golang.org/x/oauth2 v0.0.0-20210323180902-22b0adad7558
	golang.org/x/sys v0.0.0-20210326220804-49726bf1d181
	golang.org/x/text v0.3.5 // indirect
	google.golang.org/api v0.42.0
	google.golang.org/appengine v1.6.7 // indirect
	google.golang.org/genproto v0.0.0-20210323160006-e668133fea6a // indirect
	google.golang.org/grpc v1.36.0 // indirect
	google.golang.org/protobuf v1.26.0 // indirect
)
